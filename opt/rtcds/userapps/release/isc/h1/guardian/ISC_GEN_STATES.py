# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: ISC_GEN_STATES.py 9988 2015-03-07 08:00:07Z sheila.dwyer@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/isc/h1/guardian/ISC_GEN_STATES.py $

import subprocess

from guardian import GuardState, GuardStateDecorator
from ISC_library import *


##################################################
# Take care of WFS centering
##################################################        
def gen_REFL_WFS_CENTERING(dof):
    class REFL_WFS_CENTERING(GuardState):
        request = False
        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def main(self):
            # input matrix
            #clear_asc_input_matrix()
            #ezca['ASC-INMATRIX_P_12_17'] = 1
            #ezca['ASC-INMATRIX_Y_12_17'] = 1
            #ezca['ASC-INMATRIX_P_13_18'] = 1
            #ezca['ASC-INMATRIX_Y_13_18'] = 1
            #ezca['ASC-DC1_P_GAIN'] = -100
            #ezca['ASC-DC1_Y_GAIN'] = -50
            #ezca['ASC-DC2_P_GAIN'] = -300
            #ezca['ASC-DC2_Y_GAIN'] = -300
            if not ASC_DC_centering_servos_OK():
                notify('REFL WFS DC centering')
                ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
                ezca['ASC-DC1_P_RSET'] = 2
                ezca['ASC-DC1_Y_RSET'] = 2
                ezca['ASC-DC2_P_RSET'] = 2
                ezca['ASC-DC2_Y_RSET'] = 2
            ezca.get_LIGOFilter('ASC-DC1_P').only_on('INPUT', 'FM1', 'FM2', 'FM10', 'LIMIT', 'OUTPUT', 'DECIMATION')
            ezca.get_LIGOFilter('ASC-DC1_Y').only_on('INPUT', 'FM1', 'FM2', 'FM10', 'LIMIT', 'OUTPUT', 'DECIMATION')
            ezca.get_LIGOFilter('ASC-DC2_P').only_on('INPUT', 'FM1', 'FM2', 'FM10', 'LIMIT', 'OUTPUT', 'DECIMATION')
            ezca.get_LIGOFilter('ASC-DC2_Y').only_on('INPUT', 'FM1', 'FM2', 'FM10', 'LIMIT', 'OUTPUT', 'DECIMATION')
            self.timer['step'] = 1

        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def run(self):
            if not ASC_DC_centering_servos_OK():
                notify('REFL WFS DC centering railed')
                ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')                
                ezca['ASC-DC1_P_RSET'] = 2
                ezca['ASC-DC1_Y_RSET'] = 2
                ezca['ASC-DC2_P_RSET'] = 2
                ezca['ASC-DC2_Y_RSET'] = 2
            # define thresholds
            reflwfs_threshold_dc = 0.5 # threshold on the WFS centering
            # wait for centering to reach a reasonable value
            err = ezcaAverageMultiple(('ASC-DC1_P_INMON', 'ASC-DC1_Y_INMON', 'ASC-DC2_P_INMON', 'ASC-DC2_Y_INMON'))
            log(max(abs(err)))
            #log(err)
            if max(abs(err)) > reflwfs_threshold_dc:
                if self.timer['step']:
                    err = ezcaAverageMultiple(('ASC-DC1_P_INMON', 'ASC-DC1_Y_INMON', 'ASC-DC2_P_INMON', 'ASC-DC2_Y_INMON'))
                    self.timer['step'] = 1
            else:
                return True
    return REFL_WFS_CENTERING

def gen_ENGAGE_CORNER_WFS_CENTERING(dof):
    class ENGAGE_CORNER_WFS_CENTERING(GuardState):
            request = False
            @assert_mc_locked
            @assert_dof_locked_gen(dof)
            #@get_subordinate_watchdog_check_decorator(self.nodes)
            #@nodes.checker()
            def main(self):
                if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
                    notify('Toast is ready!')
                    ezca['ISI-HAM6_WD_RSET'] = 1
                    time.sleep(1)
                    ezca['ISI-HAM6_DACKILL_RESET'] = 1
                    ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1 # open fast shutter  
                # input matrix
                log('turning on DC centering')
                #clear_asc_input_matrix()
                #ezca['ASC-INMATRIX_P_12_17'] = 1
                #ezca['ASC-INMATRIX_Y_12_17'] = 1
                #ezca['ASC-INMATRIX_P_13_18'] = 1
                #ezca['ASC-INMATRIX_Y_13_18'] = 1
                ezca['ASC-INMATRIX_P_14_19'] = 1  # AS_A --> DC3 --> OM2
                ezca['ASC-INMATRIX_Y_14_19'] = 1
                ezca['ASC-INMATRIX_P_15_20'] = 1  # AS_B --> DC4 --> OM1
                ezca['ASC-INMATRIX_Y_15_20'] = 1
                ezca.get_LIGOFilter('ASC-DC1_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC1_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_Y').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_P').switch_on('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_Y').switch_on('INPUT')
                self.timer['wait'] = 1
                #ezca.get_LIGOFilter('ASC-DC3_P').only_on('INPUT', 'FM6', 'FM8', 'FM10', 'LIMIT', 'OUTPUT', 'DECIMATION')  # Turn on 1/f shaping, lowpass, comb60
                #ezca.get_LIGOFilter('ASC-DC3_Y').only_on('INPUT', 'FM6', 'FM8', 'FM10', 'LIMIT', 'OUTPUT', 'DECIMATION')
                #ezca.get_LIGOFilter('ASC-DC4_P').only_on('INPUT', 'FM6', 'FM8', 'FM10', 'LIMIT', 'OUTPUT', 'DECIMATION')
                #ezca.get_LIGOFilter('ASC-DC4_Y').only_on('INPUT', 'FM6', 'FM8', 'FM10', 'LIMIT', 'OUTPUT', 'DECIMATION')
                #ezca['ASC-DC1_P_GAIN'] = -100
                #ezca['ASC-DC1_Y_GAIN'] = -50
                #ezca['ASC-DC2_P_GAIN'] = -300
                #ezca['ASC-DC2_Y_GAIN'] = -300
                #ezca['ASC-DC3_P_GAIN'] = 30
                #ezca['ASC-DC3_Y_GAIN'] = -20
                #ezca['ASC-DC4_P_GAIN'] = -30  
                #ezca['ASC-DC4_Y_GAIN'] = -10
                if not CORNER_WFS_DC_centering_servos_OK():  
                    notify('REFL or AS WFS DC centering')
                    ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC3_P').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC3_Y').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC4_P').switch_off('INPUT')
                    ezca.get_LIGOFilter('ASC-DC4_Y').switch_off('INPUT')
                    ezca['ASC-DC1_P_RSET'] = 2
                    ezca['ASC-DC1_Y_RSET'] = 2
                    ezca['ASC-DC2_P_RSET'] = 2
                    ezca['ASC-DC2_Y_RSET'] = 2
                    ezca['ASC-DC3_P_RSET'] = 2
                    ezca['ASC-DC3_Y_RSET'] = 2
                    ezca['ASC-DC4_P_RSET'] = 2
                    ezca['ASC-DC4_Y_RSET'] = 2

                    self.timer['wait'] = 1
                    ezca.get_LIGOFilter('ASC-DC1_P').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC1_Y').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC2_P').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC2_Y').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC3_P').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC3_Y').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC4_P').switch_on('INPUT')
                    ezca.get_LIGOFilter('ASC-DC4_Y').switch_on('INPUT')
                #ezca.get_LIGOFilter('ASC-DC4_P').switch_on('INPUT', 'FM1',  'FM3', 'FM5', 'LIMIT', 'OUTPUT', 'DECIMATION')
                #ezca.get_LIGOFilter('ASC-DC4_Y').switch_on('INPUT', 'FM1',  'FM3', 'FM5', 'LIMIT', 'OUTPUT', 'DECIMATION')
                    self.timer['wait'] = 1
                #ezca.get_LIGOFilter('ASC-DC3_P').switch_on('INPUT', 'FM1')
                #ezca.get_LIGOFilter('ASC-DC3_Y').switch_on('INPUT', 'FM1')

            def run(self):
                #if self.timer['wait']:
                #    ezca.get_LIGOFilter('ASC-DC1_P').switch_on('FM1')
                #    ezca.get_LIGOFilter('ASC-DC1_Y').switch_on('FM1')
                #    ezca.get_LIGOFilter('ASC-DC2_P').switch_on('FM1')
                #    ezca.get_LIGOFilter('ASC-DC2_Y').switch_on('FM1')
                return True
                    
    return ENGAGE_CORNER_WFS_CENTERING



def gen_CORNER_WFS_CENTERING(dof):
    class CORNER_WFS_CENTERING(GuardState):
        request = False
        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(self.nodes)
        #@nodes.checker()
        def main(self):
            self.timer['step'] = 1

        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(self.nodes)
        #@nodes.checker()
        def run(self):
            if not CORNER_WFS_DC_centering_servos_OK():
                notify('REFL or AS WFS DC centering railed')
                ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_Y').switch_off('INPUT')
                ezca['ASC-DC1_P_RSET'] = 2
                ezca['ASC-DC1_Y_RSET'] = 2
                ezca['ASC-DC2_P_RSET'] = 2
                ezca['ASC-DC2_Y_RSET'] = 2
                ezca['ASC-DC3_P_RSET'] = 2
                ezca['ASC-DC3_Y_RSET'] = 2
                ezca['ASC-DC4_P_RSET'] = 2
                ezca['ASC-DC4_Y_RSET'] = 2
                self.timer['wait'] = 1
                ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC3_Y').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_P').switch_off('INPUT')
                ezca.get_LIGOFilter('ASC-DC4_Y').switch_off('INPUT')
            # define thresholds
            threshold_dc = 0.2 # threshold on the WFS centering
            # wait for centering to reach a reasonable value
            err = ezcaAverageMultiple(('ASC-DC1_P_INMON', 'ASC-DC1_Y_INMON', 'ASC-DC2_P_INMON', 'ASC-DC2_Y_INMON', 'ASC-DC3_P_INMON', 'ASC-DC3_Y_INMON', 'ASC-DC4_P_INMON', 'ASC-DC4_Y_INMON'))
            if max(abs(err)) > threshold_dc:
                if self.timer['step']:
                    err = ezcaAverageMultiple(('ASC-DC1_P_INMON', 'ASC-DC1_Y_INMON', 'ASC-DC2_P_INMON', 'ASC-DC2_Y_INMON', 'ASC-DC3_P_INMON', 'ASC-DC3_Y_INMON', 'ASC-DC4_P_INMON', 'ASC-DC4_Y_INMON'))
                    self.timer['step'] = 1
            else:
                return True
    return CORNER_WFS_CENTERING


def gen_OFFLOAD_ALIGNMENT(dof):
    class OFFLOAD_ALIGNMENT(GuardState):
        request = True
        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def main(self):

            old_gain_p = [None]*len(self.optics)
            old_gain_y = [None]*len(self.optics)
            ramp_time = 20.0

            for i in range(len(self.optics)):
                ezca['SUS-' + self.optics[i]+'_' + self.stage[i] + '_LOCK_P_TRAMP'] = ramp_time
                ezca['SUS-' + self.optics[i]+'_' + self.stage[i] + '_LOCK_Y_TRAMP'] = ramp_time

            #offload
            for i in range(len(self.optics)):
                offload(self.optics[i], self.stage[i])

            return True

        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def run(self):
            flag = False
            for i in range(len(self.optics)):            
                if ezca['GRD-SUS_%s_NOTIFICATION'%self.optics[i]]:
                    notify('SAVE %s'%self.optics[i])
                    flag = True
            if flag == False:
                return True
    return OFFLOAD_ALIGNMENT

# offload for M2
def gen_OFFLOAD_ALIGNMENT2(dof):
    class OFFLOAD_ALIGNMENT2(GuardState):
        request = True
        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        def main(self):
            for j in range(len(self.loops)):
                # hold outputs
                ezca.get_LIGOFilter('ASC-%s'%self.loops[j]).switch_on('HOLD')

            #offload

            old_gain_p = [None]*len(self.optics)
            old_gain_y = [None]*len(self.optics)

            
            for i in range(len(self.optics)):
                old_gain_p[i],old_gain_y[i], fault = offload_M2_cheap(self.optics[i])
                if fault:
                    log('OFFSET NOT POSSIBLE')
                    return False

            for j in range(len(self.loops)):
                # turn off the loops
                ezca.get_LIGOFilter('ASC-%s'%self.loops[j]).switch_off('INPUT')            
                #clear history
                ezca['ASC-%s_RSET'%self.loops[j]] = 2
                # unhold outputs
                ezca.get_LIGOFilter('ASC-%s'%self.loops[j]).switch_off('HOLD')
            for i in range(len(self.optics)):
                ezca['SUS-' + self.optics[i] + '_M2_LOCK_Y_GAIN'] = old_gain_y[i]
                ezca['SUS-' + self.optics[i] + '_M2_LOCK_P_GAIN'] = old_gain_p[i]

            for j in range(len(self.loops)):
                # turn on the loops
                ezca.get_LIGOFilter('ASC-%s'%self.loops[j]).switch_on('INPUT')  
            return True

        @assert_mc_locked
        #@assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def run(self):
            flag = False
            for i in range(len(self.optics)):            
                if ezca['GRD-SUS_%s_NOTIFICATION'%self.optics[i]]:
                    notify('SAVE %s'%self.optics[i])
                    flag = True
            if flag == False:
                return True
    return OFFLOAD_ALIGNMENT2

# isnt this the same of the first offload??
def gen_OFFLOAD_ALIGNMENT3(dof):
    class OFFLOAD_ALIGNMENT3(GuardState):
        request = True
        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        def main(self):
            scriptPath = '/opt/rtcds/userapps/release/isc/h1/guardian/offloadOpticAlign.py'
            self.procList = []
            for ii, optic in enumerate(self.optics):
                notify('Now offloading {}'.format(optic))
                opticOffload = subprocess.Popen([scriptPath, '-o', optic],
                                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                self.procList.append(opticOffload)

        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        def run(self):
            return True
    return OFFLOAD_ALIGNMENT3

# a more gentle offload of M1(??), this pauses the asc instead of letting them run
def gen_OFFLOAD_ALIGNMENT4(dof):
    class OFFLOAD_ALIGNMENT4(GuardState):
        request = True
        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        def main(self):
            for j in range(len(self.loops)):
                # hold outputs
                ezca.get_LIGOFilter('ASC-%s'%self.loops[j]).switch_on('HOLD')

            #offload

            old_gain_p = [None]*len(self.optics)
            old_gain_y = [None]*len(self.optics)

            
            for i in range(len(self.optics)):
                old_gain_p[i],old_gain_y[i] = offload_M1(self.optics[i])


            for j in range(len(self.loops)):
                # turn off the loops
                ezca.get_LIGOFilter('ASC-%s'%self.loops[j]).switch_off('INPUT')            
                #clear history
                ezca['ASC-%s_RSET'%self.loops[j]] = 2
                # unhold outputs
                ezca.get_LIGOFilter('ASC-%s'%self.loops[j]).switch_off('HOLD')
            for i in range(len(self.optics)):
                ezca['SUS-' + self.optics[i] + '_M1_LOCK_Y_GAIN'] = old_gain_y[i]
                ezca['SUS-' + self.optics[i] + '_M1_LOCK_P_GAIN'] = old_gain_p[i]

            for j in range(len(self.loops)):
                # turn on the loops
                ezca.get_LIGOFilter('ASC-%s'%self.loops[j]).switch_on('INPUT')  
            return True

        @assert_mc_locked
        #@assert_dof_locked_gen(dof)
        #@get_subordinate_watchdog_check_decorator(nodes)
        #@nodes.checker()
        def run(self):
            flag = False
            for i in range(len(self.optics)):            
                if ezca['GRD-SUS_%s_NOTIFICATION'%self.optics[i]]:
                    notify('SAVE %s'%self.optics[i])
                    flag = True
            if flag == False:
                return True
    return OFFLOAD_ALIGNMENT4
