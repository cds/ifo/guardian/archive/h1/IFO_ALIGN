# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: ISC_library.py 9988 2015-03-07 08:00:07Z sheila.dwyer@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/isc/h1/guardian/ISC_library.py $

import cdsutils
import lscparams
import alsconst
from gpstime import gpstime
tconvert = gpstime.tconvert
import numpy 

import time
##################################################
## utilities
##################################################
def grab_data(seconds,channel):
    now = int(tconvert('now').gps())
    con = cdsutils.nds.get_connection()
    buf = con.fetch(now-seconds-1,now-1,[channel])
    return buf[0].data

def offload(optic, stage):
    dofs = ['P','Y']

    control_chans = []
    offset_chans = []
    gain_chans = []
    time_ramp_chans = []


    if optic[:3]==('ETM' or 'ITM'):
        top_stage='M0'
    else:
        top_stage='M1'

    for jj in range(len(dofs)):

        control_chans.append('SUS-{0}_{1}_DRIVEALIGN_{2}_OUTMON'.format(optic, stage, dofs[jj]))
        offset_chans.append('SUS-{0}_{1}_OPTICALIGN_{2}_OFFSET'.format(optic, top_stage, dofs[jj]))
        gain_chans.append('SUS-{0}_{1}_OPTICALIGN_{2}_GAIN'.format(optic, top_stage, dofs[jj]))
        time_ramp_chans.append('SUS-{0}_{1}_OPTICALIGN_{2}_TRAMP'.format(optic, top_stage, dofs[jj]))
    drives = numpy.zeros((len(control_chans)))
    gains = numpy.zeros((len(control_chans)))
    old_sliders = numpy.empty((len(control_chans)))
    old_ramps = numpy.empty((len(control_chans)))
    new_sliders = numpy.empty((len(control_chans)))
    # We will use a series of loops over each channel to save time

    for j in range(len(dofs)):

        # Get the old slider valuesfor 
        old_sliders[j] = ezca[offset_chans[j]]

        # Get the slider gain settings
        gains[j] = ezca[gain_chans[j]]

        # Get the old ramp times
        old_ramps[j] = ezca[time_ramp_chans[j]]


    # Get the DRIVEALIGN outputs, get the OPTICALIGN gains, and add the outputs
    # to the alignment sliders

    for j in range(len(dofs)):
        # Get the DRIVEALIGN outputs; these will be offloaded to the OPTICALIGN sliders 
        #drives[j] += ezca[control_chans[j]]

        # Set the new slider values
        new_sliders[j] = old_sliders[j] + ezca[control_chans[j]] / gains[j]

        # Set the ramp time to 10 sec
        #ezca[time_ramp_chans[j]] = ramp_time

        # Set the new slider values.
        # As long as the servos are on and this is done slowly,
        # they should correct themselves!
        ezca[offset_chans[j]] = new_sliders[j]


# offloads M2 to the dither fitler bank
def offload_M2_cheap(optic):
    dofs = ['P','Y']
    fault = 0 
    ramp_time = 1

    control_chans = []
    offset_chans = []
    time_ramp_control = []
    time_ramp_offset = []
    control_gain = []
    old_gain = []
    old_offset = []
    offset_gain = []

    for jj in range(len(dofs)):
        control_chans.append('SUS-' + optic + '_M2_LOCK_' + str(dofs[jj]) + '_OUT16')
        offset_chans.append('SUS-' + optic + '_M2_DITHER_' + str(dofs[jj]) + '_OFFSET')
        time_ramp_offset.append('SUS-' + optic + '_M2_DITHER_' + str(dofs[jj]) + '_TRAMP')
        time_ramp_control.append('SUS-' + optic + '_M2_LOCK_' + str(dofs[jj]) + '_TRAMP')
        control_gain.append('SUS-' + optic + '_M2_LOCK_' + str(dofs[jj]) + '_GAIN')
        offset_gain.append('SUS-' + optic + '_M2_DITHER_' + str(dofs[jj]) + '_GAIN')


    for j in range(len(dofs)):
        old_gain.append(ezca[control_gain[j]])
        if not ezca.get_LIGOFilter('SUS-{0}_M2_DITHER_{1}'.format(optic, dofs[jj])).is_on('OFFSET'):
            notify('OFFSET IS OFF ' + optic + str(dofs[jj]))
            log('WARNING!!! OFFSET IS OFF for {0} {1}'.format(optic,ldofs[jj]))
            fault = 1
        else: 
            ezca[time_ramp_offset[j]] = ramp_time       
            ezca[time_ramp_control[j]] = ramp_time
        
            old_offset.append(ezca[offset_chans[j]])
        
            ezca[offset_chans[j]] = old_offset[j] + ezca[control_chans[j]] / ezca[offset_gain[j]]
            ezca[control_gain[j]] = 0

            time.sleep(ramp_time)
        
    old_gain.append(fault)
    return old_gain

# same as offload, except the M1 lock gain is zeroed, and history is cleared
def offload_M1(optic):
    dofs = ['P','Y']
    fault = 0 
    ramp_time = 1

    control_chans = []
    offset_chans = []
    time_ramp_control = []
    time_ramp_offset = []
    control_gain = []
    old_gain = []
    old_offset = []
    offset_gain = []
    reset = []

    for jj in range(len(dofs)):
        control_chans.append('SUS-' + optic + '_M1_LOCK_' + str(dofs[jj]) + '_OUT16')
        offset_chans.append('SUS-' + optic + '_M1_OPTICALIGN_' + str(dofs[jj]) + '_OFFSET')
        time_ramp_offset.append('SUS-' + optic + '_M1_OPTICALIGN_' + str(dofs[jj]) + '_TRAMP')
        time_ramp_control.append('SUS-' + optic + '_M1_LOCK_' + str(dofs[jj]) + '_TRAMP')
        control_gain.append('SUS-' + optic + '_M1_LOCK_' + str(dofs[jj]) + '_GAIN')
        offset_gain.append('SUS-' + optic + '_M1_OPTICALIGN_' + str(dofs[jj]) + '_GAIN')
        reset.append('SUS-' + optic + '_M1_LOCK_' + str(dofs[jj]) + '_RSET') 


    for j in range(len(dofs)):
        old_gain.append(ezca[control_gain[j]])
        ezca[time_ramp_offset[j]] = ramp_time       
        ezca[time_ramp_control[j]] = ramp_time
        
        old_offset.append(ezca[offset_chans[j]])
        
        ezca[offset_chans[j]] = old_offset[j] + ezca[control_chans[j]] / ezca[offset_gain[j]]
        ezca[control_gain[j]] = 0

        time.sleep(ramp_time)
        ezcca[reset[j]] = 2
        
    return old_gain


def clear_asc_input_matrix():
    for jj in range(1, 28):
        for ii in range(1, 17):
            ezca['ASC-INMATRIX_P_%d_%d'%(ii,jj)] = 0 
            ezca['ASC-INMATRIX_Y_%d_%d'%(ii,jj)] = 0 

#################################################
# centering function using GigE camera
#################################################
def GigE_centering(actuation_name, sensor_name, target_value, rate_value): 
    # move actuation point
    ezca[actuation_name] += rate_value * (target_value - ezca[sensor_name])
    # return the distance to the target
    return ezca[sensor_name] - target_value
##################################################
# check for lock
##################################################
from guardian import GuardState, GuardStateDecorator

def MC_locked():
    return (ezca.read('GRD-IMC_LOCK_STATE', as_string=True) == 'LOCKED' or ezca.read('GRD-IMC_LOCK_STATE', as_string=True) == 'ISS_ON')

# Only use POP18I, that way this doesn't screw it up if the user wants to tweak just the 
# PRMI before moving on to the DRMI.
def DRMI_locked():
    #log('checking DRMI lock')
    return ezca['LSC-MICH_TRIG_MON'] and ezca['LSC-PRCL_TRIG_MON'] and ezca['LSC-SRCL_TRIG_MON']

def PRMI_locked():
    if (ezca['LSC-MICH_TRIG_MON'] and ezca['LSC-PRCL_TRIG_MON']):
        log('PRMI locked')
        return True
    else:
        return False

def ARM_IR_locked(arm):
    if ezca['LSC-%sARM_FM_TRIG_MON'%arm]:
        log('arm IR locked')
        return True
    else:
        log('arm not IR locked')
        return False

# PRXY lock checker
def PRXY_locked():
    log('checking PR lock')
    return cdsutils.avg(1, 'LSC-ASAIR_A_LF_NORM_MON') > lscparams.prxy_asair_lock_threshold

def PRXY_oscillating():
    return cdsutils.avg(1, 'LSC-POPAIR_A_LF_OUT_DQ', True)[1] >= lscparams.prxy_popairdc_oscillating_thresh

def arm_quiet():
    return ((cdsutils.avg(1,'ALS-C_TRX_A_LF_OUT_DQ', True)[1] <= alsconst.arm_quiet_thresh)
             and (cdsutils.avg(1,'ALS-C_TRY_A_LF_OUT_DQ', True)[1] <= alsconst.arm_quiet_thresh)
                and ezca['ALS-C_TRX_A_LF_OUTMON'] >= 0.9 and ezca['ALS-C_TRY_A_LF_OUTMON'] >= 0.9)

def SRXY_locked():
    return ezca['LSC-ASAIR_A_LF_NORM_MON'] >= lscparams.sry_locked_threshold

def SRM_sus_saturating():
    # Check the 9th bit of the FEC state word for SRM front end,
    # which indicates overflows in the DAC outputs.
    return int(ezca['FEC-45_STATE_WORD']) & 1 << 9

def SR2_sus_saturating():
    # Check the 9th bit of the FEC state word for SRM front end,
    # which indicates overflows in the DAC outputs
    return int(ezca['FEC-41_STATE_WORD']) & 1 << 9

def MICHDARK_locked():
    return cdsutils.avg(1, 'LSC-ASAIR_A_LF_NORM_MON') <= lscparams.michdark_locked_threshold

def MICHBRIGHT_locked():
    return cdsutils.avg(1,'LSC-ASAIR_A_LF_NORM_MON')  >= lscparams.michbright_locked_threshold

def is_locked(dof):
    if dof == 'YARM':
        return ARM_IR_locked('Y')    
    if dof == 'XARM':
        return ARM_IR_locked('X')
    if dof == 'PRX':
        return PRXY_locked()
    if dof == 'PRY':
        return PRXY_locked()
    if dof == 'SRX':
        return PRXY_locked()
    if dof == 'SRY':
        return SRXY_locked()
    if dof == 'PRMI':
        return PRMI_locked()
    if dof == 'MICH_DARK':
        return MICHDARK_locked()
    if dof == 'DRMI':
        return DRMI_locked()

#################################################
# check for error conditions
#################################################
def DRMI_aligned():
        if not ezca.read('GRD-SUS_PRM_STATE_S', as_string=True) == 'ALIGNED':
            return False
        elif not ezca.read('GRD-SUS_PR2_STATE_S', as_string=True) == 'ALIGNED':
            return False
        elif not ezca.read('GRD-SUS_PR3_STATE_S', as_string=True) == 'ALIGNED':
            return False
        elif not ezca.read('GRD-SUS_SRM_STATE_S', as_string=True) == 'ALIGNED':
            return False
        elif not ezca.read('GRD-SUS_SR2_STATE_S', as_string=True) == 'ALIGNED':
            return False
        elif not ezca.read('GRD-SUS_SR3_STATE_S', as_string=True) == 'ALIGNED':
            return False
        elif not ezca.read('GRD-SUS_ITMY_STATE_S', as_string=True) == 'ALIGNED':
            return False
        elif not ezca.read('GRD-SUS_ITMX_STATE_S', as_string=True) == 'ALIGNED':
            return False        
        #elif not ezca.read('GRD-SUS_ETMY_STATE_S', as_string=True) == 'MISALIGNED':
        #    return False
        #elif not ezca.read('GRD-SUS_ETMX_STATE_S', as_string=True) == 'MISALIGNED':
        #    return False
        elif not ezca.read('GRD-SUS_BS_STATE_S', as_string=True) == 'ALIGNED':
            return False
        else:
            return True

def BS_opLev_OK(lscparams):
    oplev_y_avg = cdsutils.avg(1, 'SUS-BS_M3_OPLEV_YAW_OUTPUT', True)  # this needs to be changed, why should the dc value indicate an oscillation?  should this be the stdev?
    oplev_p_avg = cdsutils.avg(1, 'SUS-BS_M3_OPLEV_PIT_OUTPUT', True)
    if oplev_p_avg == lscparams.bs_oscillation_thresh and oplev_y_avg == lscparams.bs_oscillation_thresh:
        # equality, AND statement to keep this from happening
        notify('BS oscillation WHYWHYWHY')
        return True
    else:
        return True

def input_matrix_OK():
    flag = 1
    smallNum = 1e-2
    numRows = 8
    numCols = 29
    wait_msg = 'Waiting for input matrix elements ';
    for ii in range(1, numRows):
        for jj in range(1, numCols):
            matrixValue = ezca['LSC-PD_DOF_MTRX_{}_{}'.format(ii,jj)]
            settingValue = ezca['LSC-PD_DOF_MTRX_SETTING_{}_{}'.format(ii,jj)]
            if not abs(matrixValue-settingValue) / (abs(settingValue)+smallNum) < smallNum:
                wait_msg += '({0}, {1})'.format(ii, jj)
                flag = 0
    if not flag:
        notify(wait_msg)
    return flag

def rampMatrixOk(matrixName, chansList):
    """
    matrixName is something like 'LSC-PD_DOF_MTRX'

    chansList is a list of ordered pairs corresponding to channel names; e.g.,
    [(2, 4), (3, 13)]
    """
    matrixGood = True
    smallNum = 1e-2
    for pair in chansList:
        matrixVal = ezca[matrixName+'_{0}_{1}'.format(*pair)]
        settingVal = ezca[matrixName+'_SETTING_{0}_{1}'.format(*pair)]
        if not (abs(matrixVal - settingVal) / (abs(settingVal) + smallNum) < smallNum):
            matrixGood = False
    return matrixGood

def loadRampMatrix(matrixName, chansList, numTries):
    """
    matrixName is something like 'LSC-PD_DOF_MTRX'

    chansList is a list of ordered pairs corresponding to channel names;
    e.g., [(2, 4), (3, 13)]
    """
    rampTime = ezca[matrixName+'_TRAMP']
    ezca[matrixName+'_LOAD_MATRIX'] = 1
    time.sleep(rampTime+1)
    ii = 0
    while ii < numTries:
        matrixGood = rampMatrixOk(matrixName, chansList)
        if matrixGood:
            break
        else:
            notify("Pushing the 'load matrix' button again...")
            ezca[matrixName+'_LOAD_MATRIX'] = 1
            time.sleep(rampTime+1)
        ii += 1
    return matrixGood
        

def ASC_DC_centering_servos_OK():
    if ezca['ASC-WFS_GAIN'] < 0.1:
        notify('ASC MASTER GAIN')
    return not ((abs(ezca['ASC-DC1_P_OUTPUT']) >=  ezca['ASC-DC1_P_LIMIT']
                 or abs(ezca['ASC-DC1_Y_OUTPUT']) >=  ezca['ASC-DC1_Y_LIMIT']
                 or abs(ezca['ASC-DC2_P_OUTPUT']) >=  ezca['ASC-DC2_P_LIMIT']
                 or abs(ezca['ASC-DC2_Y_OUTPUT']) >=  ezca['ASC-DC2_Y_LIMIT']))

def CORNER_WFS_DC_centering_servos_OK():
    if ezca['ASC-WFS_GAIN'] < 0.1:
        notify('ASC MASTER GAIN')
    return not ((abs(ezca['ASC-DC1_P_OUTPUT']) >= ezca['ASC-DC1_P_LIMIT']
                 or abs(ezca['ASC-DC1_Y_OUTPUT']) >= ezca['ASC-DC1_Y_LIMIT']
                 or abs(ezca['ASC-DC2_P_OUTPUT']) >= ezca['ASC-DC2_P_LIMIT']
                 or abs(ezca['ASC-DC2_Y_OUTPUT']) >= ezca['ASC-DC2_Y_LIMIT']
                 or abs(ezca['ASC-DC3_P_OUTPUT']) >= ezca['ASC-DC3_P_LIMIT']
                 or abs(ezca['ASC-DC3_Y_OUTPUT']) >= ezca['ASC-DC3_Y_LIMIT']
                 or abs(ezca['ASC-DC4_P_OUTPUT']) >= ezca['ASC-DC4_P_LIMIT']
                 or abs(ezca['ASC-DC4_Y_OUTPUT']) >= ezca['ASC-DC4_Y_LIMIT']))

def ezcaAverageMultiple(signals, dt=2, rate=0.05):
    """
    Average ezca data over multiple reads.

    This function is used to check the WFS centering.
    """
    # signals = list of signals
    # dt = total average time
    # rate = time beteween each subsequent sampling
    N = int(dt / rate)
    sig = numpy.zeros(len(signals))
    for i in range(N):
        for j in range(len(signals)):
            sig[j] = sig[j] + ezca[signals[j]]
        time.sleep(rate)
    sig = sig/N
    return sig

def REFL_PD_OK():
    return ezca['LSC-REFL_A_LF_OUTPUT'] <= 100

def ready_for_locking():
    message = []
    flag = False
    #check input matrix (using REFLA RF 45 I) 
    #(this is needed because of bug in ramping matrix)
    #log('checking input matrix')
    if not input_matrix_OK():
        flag = True
        message.append('Check LSC input matrix')
    #check that LSC feedback is enabled
    if not ezca.read('LSC-CONTROL_ENABLE'):
        flag = True
        message.append('LSC CONTROL disabled')
    if not ASC_DC_centering_servos_OK():
        flag = True
        message.append('Check DC centering servos') 
    if not REFL_PD_OK():
        flag = True
        message.append('No light on refl PD')       
    if flag:
        notify(', '.join(message))
        return False
    else:
        return True

def ready_for_locking_MICH():
    message = []
    flag = False
    #check input matrix (using REFLA RF 45 I) 
    #(this is needed because of bug in ramping matrix)
    if not input_matrix_OK():
        flag = True
        message.append('Check LSC input matrix')
    #check that LSC feedback is enabled
    if not ezca.read('LSC-CONTROL_ENABLE'):
        flag = True
        message.append('LSC CONTROL disabled')       
    if flag:
        notify(', '.join(message))
        return False
    else:
        return True

def ready_for_PRXY(lscparams, arm):
    message = []
    flag = False
    #check input matrix (using REFLA RF 45 I) 
    #(this is needed because of bug in ramping matrix)
    if not input_matrix_OK():
        flag = True
        message.append('check LSC input matrix')
    #check that LSC feedback is enabled
    if not ezca.read('LSC-CONTROL_ENABLE'):
        flag = True
        message.append('LSC CONTROL disabled')
    if not ASC_DC_centering_servos_OK():
        flag = True
        message.append('Refl PD, check DC centering servos')      
    if flag:
        notify(', '.join(message))
        return False
    else:
        return True

def ready_for_PRMI(lscparams):
    message = []
    flag = False
    if not input_matrix_OK():
        log('checking input matrix')
        flag = True
        message.append('Check input matrix')
    #check that LSC feedback is enabled
    if not ezca.read('LSC-CONTROL_ENABLE'):
        flag = True
        message.append('LSC CONTROL disabled') 
    if ((not ezca['LSC-POPAIR_B_RF18_WHITEN_GAIN'] == 12)
          or not ezca.get_LIGOFilter('LSC-POPAIR_B_RF18_I').is_engaged('FM9')
          or not ezca.get_LIGOFilter('LSC-POPAIR_B_RF18_I').is_engaged('FM10')
          or not ezca.get_LIGOFilter('LSC-POPAIR_B_RF18_Q').is_engaged('FM9')
          or not ezca.get_LIGOFilter('LSC-POPAIR_B_RF18_Q').is_engaged('FM10')):
        flag = True
        message.append('Check POPAIR_B_RF18 whitening filters')
    if not BS_opLev_OK(lscparams): 
        flag = True
        message.append('BS OpLev oscillating')
    if flag:
        notify(', '.join(message))
        return False
    else:
        return True

# checks the PSL input power
def PSLinputpower_OK():
    message = []
    flag = False
    if ezca['PSL-PERISCOPE_A_DC_POWERMON'] > 11000.0:
        flag = True
        message.append('PSL input power above 11W')
    elif (ezca['PSL-PERISCOPE_A_DC_POWERMON'] < 9000.0
            and ezca['PSL-ROTATIONSTAGE_POWER_REQUEST'] == 9.0):
        flag = True
        message.append('PSL input power below 9W, request 10W')
    elif (ezca['PSL-PERISCOPE_A_DC_POWERMON'] > 6000.0
            and ezca['PSL-ROTATIONSTAGE_POWER_REQUEST'] == 4.0):
        flag = True
        message.append('PSL input power above 6W, request 4W')
    elif (ezca['PSL-PERISCOPE_A_DC_POWERMON'] < 2000.0
            and ezca['PSL-ROTATIONSTAGE_POWER_REQUEST'] == 4.0):
        flag = True
        message.append('PSL input power below 2W, request 4W')
    #log(flag)    
    if flag:
        notify(', '.join(message))
        return False
    else:
        return True

#################################################
# DECORATORS
#################################################
def node_watchdog_tripped(node):
    return 'WATCHDOG_TRIPPED' in node.state

def watchdog_tripped_nodes(nodes):
    """Return list of nodes that are in tripped state."""
    tripped_nodes = []
    for node in nodes:
        if node_watchdog_tripped(node):
            tripped_nodes.append(node)
    return tripped_nodes

def get_subordinate_watchdog_check_decorator(nodes):
    class subordinate_watchdog_check(GuardStateDecorator):
        """Check that any watchdog of a subordinate node has not tripped."""
        def pre_exec(self):
            if watchdog_tripped_nodes(nodes):
                #return 'WATCHDOG_TRIPPED_SUBORDINATE'
                notify('WATCHDOG TRIPPED')
    return subordinate_watchdog_check

def unstall_nodes(nodeManager):
    class unstall_decorator(GuardStateDecorator):
        def pre_exec(self):
            for node in nodeManager.get_stalled_nodes():
                # put a check that in is in done state
                if not node.NOTIFICATION:
                    log('Unstalling ' + node.name)
                    node.revive()
    return unstall_decorator

def get_watchdog_IMC_check_decorator(nodes):
    class watchdog_IMC_check(GuardStateDecorator):
        """Check that any watchdog of a subordinate node has not tripped."""
        def pre_exec(self):
            if watchdog_tripped_nodes(nodes):
                notify('WATCHDOG TRIPPED')
            if not MC_locked():
                log('MC not locked')
                return 'LOCKLOSS'
    return watchdog_IMC_check

def get_watchdog_IMC_ARMS_check_decorator(nodes):
    class watchdog_IMC_ARMS_check(GuardStateDecorator):
        """Check that any watchdog of a subordinate node has not tripped."""
        def pre_exec(self):
            if watchdog_tripped_nodes(nodes):
                notify('WATCHDOG TRIPPED')
            if not MC_locked():
                log('MC not locked')
                return 'LOCKLOSS'
            if (nodes['ALS_XARM'].state in ['UNLOCKED', 'LOCKING', 'FAULT']
                  or nodes['ALS_YARM'].state in ['UNLOCKED', 'LOCKING', 'FAULT']):
                log('arms not locked with green')
                return 'LOCKLOSS'
    return watchdog_IMC_ARMS_check

def get_watchdog_IMC_ARMS_DRMI_check_decorator(nodes):
    class watchdog_IMC_ARMS_DRMI_check(GuardStateDecorator):
        """Check that any watchdog of a subordinate node has not tripped."""
        def pre_exec(self):
            if watchdog_tripped_nodes(nodes):
                notify('WATCHDOG TRIPPED')
            if not MC_locked():
                log('MC not locked')
                return 'LOCKLOSS'
            if (nodes['ALS_XARM'].state in ['UNLOCKED', 'LOCKING', 'FAULT']
                  or nodes['ALS_YARM'].state in ['UNLOCKED', 'LOCKING', 'FAULT']):
                log('arms not locked with green')
                return 'LOCKLOSS'
            if not DRMI_locked():
                log('DRMI lost lock')
                return 'LOCKLOSS_DRMI'
    return watchdog_IMC_ARMS_DRMI_check

def get_watchdog_IMC_DRMI_check_decorator(nodes):
    class watchdog_IMC_DRMI_check(GuardStateDecorator):
        """Check that any watchdog of a subordinate node has not tripped."""
        def pre_exec(self):
            if watchdog_tripped_nodes(nodes):
                #return 'WATCHDOG_TRIPPED_SUBORDINATE'
                notify('WATCHDOG TRIPPED')
            if not MC_locked():
                log('MC not locked')
                return 'LOCKLOSS'
            if not DRMI_locked():
                log('DRMI lost lock')
                return 'LOCKLOSS_DRMI'
            if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
                notify('TOAST IS READY!!!! turning off WFS')
                ezca.switch('ASC-DHARD_P', 'INPUT', 'OFF')
                ezca.switch('ASC-DHARD_Y', 'INPUT', 'OFF')
                
    return watchdog_IMC_DRMI_check

class assert_mc_locked(GuardStateDecorator):
    def pre_exec(self):
        if not MC_locked():
            log('MC not Locked')
            return 'DOWN'

class assert_drmi_locked(GuardStateDecorator):
    def pre_exec(self):
        if not DRMI_locked():
            return 'LOCK_DRMI_1F'

####used by ISC_configs only
class assert_xarm_IR_locked(GuardStateDecorator):
    def pre_exec(self):
        if not ARM_IR_locked('X'):
            log('arm not Locked')
            return 'DOWN'

class assert_yarm_IR_locked(GuardStateDecorator):
    def pre_exec(self):
        if not ARM_IR_locked('Y'):
            log('arm not Locked')
            return 'DOWN'

class assert_arm_IR_locked(GuardStateDecorator):
    def pre_exec(self):
        if not (ARM_IR_locked('X') or ARM_IR_locked('Y')):
            log('arm not Locked')
            return 'DOWN'


class assert_michdark_locked(GuardStateDecorator):
    def pre_exec(self):
        if not MICHDARK_locked():
            return 'DOWN'


class assert_michbright_locked(GuardStateDecorator):
    def pre_exec(self):
        if not MICHBRIGHT_locked():
            return 'LOCK_MICH_BRIGHT'

class assert_prmisb_locked(GuardStateDecorator):
    def pre_exec(self):
        if not PRMI_locked():
            return 'LOCK_PRMI_SB'

class assert_prx_locked(GuardStateDecorator):
    def pre_exec(self):
        if not PRXY_locked():
            return 'DOWN'

class assert_sry_locked(GuardStateDecorator):
    def pre_exec(self):
        if not SRXY_locked():
            return 'DOWN'

def assert_dof_locked_gen(dof):
    class assert_dof_locked(GuardStateDecorator):
        def pre_exec(self):
            if not is_locked(dof):
                if dof == 'DRMI':
                    log('la la')
                    return 'LOCK_DRMI_1F'
                else:
                    log('ta ta')
                    return 'DOWN'
    return assert_dof_locked


# LSC input matrix for the majority
intrix = cdsutils.CDSMatrix('LSC-PD_DOF_MTRX_SETTING',
		cols={'POP_A9I':1,
			'POP_A9Q':2,
			'POP_A45I':3,
			'POP_A45Q':4,
			'REFL_A9I':5,
			'REFL_A9Q':6,
			'REFL_A45I':7,
			'REFL_A45Q':8,
			'POPAIR_A9I':9,
			'POPAIR_A9Q':10,
			'POPAIR_A45I':11,
			'POPAIR_A45Q':12,
			'REFLAIR_A9I':13,
			'REFLAIR_A9Q':14,
			'REFLAIR_A45I':15,
			'REFLAIR_A45Q':16,
			'REFLAIR_B27I':17,
			'REFLAIR_B27Q':18,
			'REFLAIR_B135I':19,
			'REFLAIR_B135Q':20,
			'TRX':21,
			'TRY':22,
			'REFLSERVO_SLOW': 23,
			'ALS_COMM': 24,
			'ASAIR_ALF': 25,
			'TR_CARM':26,
			'TR_REFL9':27,
			'REFL_DC':28
			},
		rows={'DARM':1,
			'CARM':2,
			'MICH':3,
			'PRCL':4,
			'SRCL':5,
			'MCL':6,
			'XARM':7,
			'YARM':8,
			'REFLBIAS':9
			}
		)

# LSC input matrix for a minority (i.e. OMC/AS45 to DARM and CARM)
intrix_OMCAS45 = cdsutils.CDSMatrix('LSC-ARM_INPUT_MTRX_SETTING',
		cols={'OMCDC':1,
			'ASAIR_A45I':2,
			'ASAIR_A45Q':3,
			'ALS_DIFF':4,
			'REFL_DC':5
			},
		rows={'DARM':1,
			'CARM':2
			}
		)


# LSC input matrix for a minority (i.e. OMC/AS45 to nonDARM or nonCARM)
intrix_ASPDs = cdsutils.CDSMatrix('LSC-ASPD_DOF_MTRX_SETTING',
		cols={'OMCDC':1,
			'ASAIR_A45I':2,
			'ASAIR_A45Q':3,
			},
		rows={'MICH':1,
			'PRCL':2,
			'SRCL':3,
			'MCL':4,
			'XARM':5,
			'YARM':6
			}
		)

# LSC output matrix for the majority
outrix = cdsutils.CDSMatrix('LSC-OUTPUT_MTRX',
		cols={'MICH':1,
			'PRCL':2,
			'SRCL':3,
			'MCL':4,
			'XARM':5,
			'YARM':6,
			'OSC1':7,
			'OSC2':8,
			'OSC3':9,
			'MICHFF':10,
			'SRCLFF':11
			},
		rows={'ETMX':1,
			'ETMY':2,
			'ITMX':3,
			'ITMY':4,
			'PRM':5,
			'SRM':6,
			'BS':7,
			'PR2':8,
			'SR2':9,
			'MC2':10
			}
		)

# output matrix
darmcarm_outrix = cdsutils.CDSMatrix('LSC-ARM_OUTPUT_MTRX',
		cols={'DARM':1, 'CARM':2},
		rows={'ETMX':1, 'ETMY':2}
		)
