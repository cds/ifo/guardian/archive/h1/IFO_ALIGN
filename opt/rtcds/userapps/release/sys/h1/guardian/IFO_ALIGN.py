# Initial Alignment
#
# $Id$
# $HeadURL$

from guardian import GuardState, NodeManager

from LSC_CONFIGS import\
			gen_SET_ARM_IR_state

# register the nodes that you want to control
nodes = NodeManager(
		['IAS_XARM',
		'IAS_YARM',
		'IAS_PRC',
		'IAS_MICH',
		'IAS_SRC',
		'IAS_INPUT',
		'SUS_PRM',
		'SUS_SRM',
		'SUS_ITMX',
		'SUS_ITMY',
		'SUS_ETMX',
		'SUS_ETMY'
])


nominal = 'IDLE'

# init
#################################################
class INIT(GuardState):
    def main(self):
        nodes.set_managed()
        return

# idle  (meaningless ?)
#################################################
class IDLE(GuardState):
    request = True
    def main (self):
        pass
        return


# no feedback to cut the LSC and ASC signals
#################################################
class NO_FEEDBACK(GuardState):
    request = False
    goto = True
    #[FIXME] please include the ASC stuff as well
    def main (self):
        # stop the LSC feedback
        for DOF in ('DARM', 'CARM', 'XARM', 'YARM', 'PRCL', 'MICH', 'SRCL'):
            log('shutting the LSC feedaback')
            ezca.switch('LSC-%s'%(DOF), 'OUTPUT', 'OFF')
        # put the small guarians back to the IDLE state
        for DOF in ('XARM', 'YARM', 'INPUT', 'PRC', 'MICH', 'SRC'):
            log('making all the slave guardians to IDLE')
            nodes['IAS_%s'%DOF] =  'IDLE'
        return True


# SET_XARM 
#################################################
SET_XARM = gen_SET_ARM_IR_state('X')

# LOCK_XARM 
#################################################
def gen_LOCK_XARM():
	class LOCK_XARM(GuardState):
		request = False
		def main(self):
			nodes['IAS_XARM']= 'LOCKED'
		def run(self):
			if nodes.arrive:
				return True
	return LOCK_XARM


LOCK_XARM = gen_LOCK_XARM()

# ALIGN_INPUT
#################################################
class ALIGN_INPUT(GuardState):
	request = False
	def main(self):
		nodes['IAS_XARM'] = 'INPUT_ALIGN'
	def run(self):
		if nodes.arrive:
			return True


		


#################################################

edges = [
# starting points
    ('NO_FEEDBACK', 'IDLE'),
#    ('IDLE', 'FORCE_RESTORE_ALL_SUS'),
# input
    ('IDLE', 'SET_XARM'),
	('SET_XARM', 'LOCK_XARM'),
	('LOCK_XARM', 'ALIGN_INPUT')
#    ('SET_XARM', 'ALIGNING_INPUT'),
#    ('ALIGNING_INPUT', 'INPUT_ALIGNED'),
# MICH
#    ('IDLE', 'PREPARE_MICH'),
#    ('PREPARE_MICH', 'ALIGNING_MICH'),
#    ('ALIGNING_MICH', 'MICH_ALIGNED'),
# PRX
#    ('PREPARE_PRX', 'ALIGNING_PRC'),
#    ('ALIGNING_PRC', 'PRC_ALIGNED'),
# SRY
#    ('IDLE', 'PREPARE_SRY'),
#    ('PREPARE_SRY', 'ALIGNING_SRC'),
#    ('ALIGNING_SRC', 'SRC_ALIGNED')
# DRMI
#    ('IDLE', 'PREPARE_PRX'),
#    ('PREPARE_PRX', 'ALIGNING_PRC_DRMI'),
#    ('ALIGNING_PRC_DRMI', 'PREPARE_MICH_DRMI'),
#    ('PREPARE_MICH_DRMI', 'ALIGNING_MICH_DRMI'),
#    ('ALIGNING_MICH_DRMI', 'PREPARE_SRY_DRMI'),
#    ('PREPARE_SRY_DRMI', 'ALIGNING_SRC_DRMI'),
#    ('ALIGNING_SRC_DRMI','DRMI_ALIGNED'),
# FULL IFO
#    ('IDLE', 'MISALIGN_ETMS__FULL_IFO'),
#    ('MISALIGN_ETMS__FULL_IFO', 'ALIGNING_DUAL_TMSS_FULL_IFO'),
#    ('ALIGNING_DUAL_TMSS_FULL_IFO', 'ALIGNING_DUAL_ITMS_FULL_IFO'),
#    ('ALIGNING_DUAL_ITMS_FULL_IFO', 'RESTORE_ETMS_FULL_IFO'),
#    ('RESTORE_ETMS_FULL_IFO', 'ALIGNING_DUAL_ETMS_FULL_IFO'),
#    ('ALIGNING_DUAL_ETMS_FULL_IFO', 'DUAL_ARMS_ALIGNED_FULL_IFO'),
#    ('DUAL_ARMS_ALIGNED_FULL_IFO', 'PREPARE_XARM_FULL_IFO'),
#    ('PREPARE_XARM_FULL_IFO', 'ALIGNING_INPUT_FULL_IFO'),
#    ('ALIGNING_INPUT_FULL_IFO', 'INPUT_ALIGNED_FULL_IFO'),
#    ('INPUT_ALIGNED_FULL_IFO', 'PREPARE_PRX_FULL_IFO'),
#    ('PREPARE_PRX_FULL_IFO', 'ALIGNING_PRC_FULL_IFO'),
#    ('ALIGNING_PRC_FULL_IFO', 'PRC_ALIGNED_FULL_IFO'),
#    ('PRC_ALIGNED_FULL_IFO', 'PREPARE_MICH_FULL_IFO'),
#    ('PREPARE_MICH_FULL_IFO', 'ALIGNING_MICH_FULL_IFO'),
#    ('ALIGNING_MICH_FULL_IFO', 'MICH_ALIGNED_FULL_IFO'),
#    ('MICH_ALIGNED_FULL_IFO', 'PREPARE_SRY_FULL_IFO'),
#    ('PREPARE_SRY_FULL_IFO', 'ALIGNING_SRC_FULL_IFO'),
#    ('ALIGNING_SRC_FULL_IFO', 'SRC_ALIGNED_FULL_IFO'),
#    ('SRC_ALIGNED_FULL_IFO', 'RESTORE_ALL'),
#    ('RESTORE_ALL', 'IFO_ALIGNED'),
# DUAL arms
#    ('IDLE','MISALIGN_ETMS_DUAL_ARMS'),
#    ('MISALIGN_ETMS_DUAL_ARMS', 'ALIGNING_DUAL_TMSS'),
#    ('ALIGNING_DUAL_TMSS', 'ALIGNING_DUAL_ITMS'),
#    ('ALIGNING_DUAL_ITMS', 'RESTORE_ETMS'),
#    ('RESTORE_ETMS', 'ALIGNING_DUAL_ETMS'),
#    ('ALIGNING_DUAL_ETMS', 'DUAL_ARMS_ALIGNED'),
]
        
